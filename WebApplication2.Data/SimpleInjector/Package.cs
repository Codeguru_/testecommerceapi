﻿using System;
using SimpleInjector;
using SimpleInjector.Packaging;
using WebApplication2.Data.Repositories;

namespace WebApplication2.Data.SimpleInjector
{
    public class DataSimpleInjectorPackage : IPackage
    {
        public static string ItemsDatabasePath { get; set; }
        public static string AdminDatabasePath { get; set; }

        public void RegisterServices(Container container)
        {
            container.Register<ICartItemsRepository, CartItemsRepository>(Lifestyle.Singleton);
            container.Register<IUserDetailsRepository>(() => new UserDetailsRepository(AdminDatabasePath), Lifestyle.Singleton);
            container.Register<IItemsRepository>(() => new ItemsRepository(ItemsDatabasePath), Lifestyle.Singleton);
        }
    }
}
