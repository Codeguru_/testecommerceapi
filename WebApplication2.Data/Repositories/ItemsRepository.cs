﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApplication2.Core.Models;

namespace WebApplication2.Data.Repositories
{
    public class ItemsRepository : JsonFileRepository<Item>, IItemsRepository
    {
        public ItemsRepository(string databasePath) : base(databasePath)
        {
        }

        Item IItemsRepository.Add(Item item)
        {
            item.Id = 1;
            List<long> existingIds = Values?.Select(x => x.Id).ToList();
            if (existingIds != null && existingIds.Any())
            {
                var i = 1;
                for (; i <= existingIds.Count && i == existingIds[i - 1]; i++)
                {
                }
                item.Id = i;
            }
            Values.Add(item);
            SaveChanges();
            return item;
        }

        void IItemsRepository.Remove(object itemId)
        {
            var existingItem = Values.FirstOrDefault(x => itemId.Equals(x.Id)) ?? throw new InvalidOperationException($"Item with id {itemId} was not found in database");
            Values.Remove(existingItem);
            SaveChanges();
        }

        void IItemsRepository.Remove(Item item)
        {
            ((IItemsRepository)this).Remove(item.Id);
            SaveChanges();
        }

        Item IItemsRepository.Find(object itemId)
        {
            return Values.FirstOrDefault(x => itemId.Equals(x.Id));
        }

        IEnumerable<Item> IItemsRepository.GetAll()
        {
            return Values.ToList();
        }
    }
}