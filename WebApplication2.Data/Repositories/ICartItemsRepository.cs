﻿using System;
using System.Collections.Generic;
using WebApplication2.Core.Models;

namespace WebApplication2.Data.Repositories
{
    public interface ICartItemsRepository
    {
        CartItem AddOrUpdate(long cartId, long itemId, int quantity);
        IEnumerable<CartItem> GetAll(long cartId);
        void Remove(long cartId, long itemId);
        void RemoveAll(long cartId);
    }
}
