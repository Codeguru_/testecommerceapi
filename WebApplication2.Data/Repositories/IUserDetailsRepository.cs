﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApplication2.Core.Models;

namespace WebApplication2.Data.Repositories
{
    public interface IUserDetailsRepository
    {
        void Add(UserDetails user);        
        UserDetails Update(UserDetails user);
        void Remove(UserDetails user);
        void Remove(long userId);
        UserDetails Find(long userId);
        UserDetails Find(Expression<Func<UserDetails, bool>> filter);
        IEnumerable<UserDetails> GetAll();
        IEnumerable<UserDetails> FindAll(Expression<Func<UserDetails, bool>> filter);
    }
}
