﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApplication2.Core.Models;

namespace WebApplication2.Data.Repositories
{
    public class CartItemsRepository : ICartItemsRepository
    {
        private static readonly IDictionary<long, IList<CartItem>> CartItemsDbContext = new Dictionary<long, IList<CartItem>>();

        CartItem ICartItemsRepository.AddOrUpdate(long cartId, long itemId, int quantity)
        {
            var newCardItem = new CartItem(cartId, itemId, quantity);
            if (CartItemsDbContext.ContainsKey(cartId))
            {
                var existingItem = CartItemsDbContext[cartId].FirstOrDefault(x => x.ItemId == itemId);
                if (existingItem != null)
                {
                    existingItem.Quantity = quantity;
                    return existingItem;
                }
                CartItemsDbContext[cartId].Add(newCardItem);
            }
            else
            {
                CartItemsDbContext.Add(cartId, new List<CartItem>
                {
                    newCardItem
                });
            }

            return newCardItem;
        }

        IEnumerable<CartItem> ICartItemsRepository.GetAll(long cartId)
        {
            return CartItemsDbContext.ContainsKey(cartId)? CartItemsDbContext[cartId]: null;
        }

        void ICartItemsRepository.Remove(long cartId, long itemId)
        {
            if (!CartItemsDbContext.ContainsKey(cartId))
            {
                throw new InvalidOperationException("Cart with id specified was not found in database");
            }

            var existingItem = CartItemsDbContext[cartId].FirstOrDefault(x => x.ItemId == itemId);

            if (existingItem == null)
            {
                throw new InvalidOperationException("Item with id specified was not found in database");
            }

            CartItemsDbContext[cartId].Remove(existingItem);
        }

        void ICartItemsRepository.RemoveAll(long cartId)
        {
            if (!CartItemsDbContext.ContainsKey(cartId))
            {
                throw new InvalidOperationException("Cart with id specified was not found in database");
            }
            CartItemsDbContext[cartId].Clear();
        }
    }
}
