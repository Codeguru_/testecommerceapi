﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using WebApplication2.Core.Extensions;
using WebApplication2.Core.Models;

namespace WebApplication2.Data.Repositories
{
    public class UserDetailsRepository : JsonFileRepository<UserDetails>, IUserDetailsRepository
    {
        public UserDetailsRepository(string userDbPath) : base(userDbPath)
        {
        }

        void IUserDetailsRepository.Add(UserDetails user)
        {
            var existingUser = ((IUserDetailsRepository) this).Find(x => x.Email.Equals(user.Email, StringComparison.InvariantCultureIgnoreCase));
            if (existingUser != null)
            {
                throw new InvalidOperationException("User already exists in database");
            }

            user.DateRegisteredTimeStamp = DateTime.UtcNow.ToUnixTimeStamp();
            user.Id = 1;
            List<long> existingIds = Values?.Select(x => x.Id).ToList();
            if (existingIds != null && existingIds.Any())
            {
                var i = 1;
                for (; i <= existingIds.Count && i == existingIds[i - 1]; i++)
                {
                }
                user.Id = i;
            }

            user.SecurityStamp = Guid.NewGuid().ToString();

            Values.Add(user);
            SaveChanges();
        }

        UserDetails IUserDetailsRepository.Update(UserDetails user)
        {
            var existingUser = ((IUserDetailsRepository)this).Find(user.Id);
            if (existingUser == null)
            {
                throw new InvalidOperationException($"User with id {user.Id} does not exist in database");
            }

            existingUser.Email = user.Email;
            existingUser.Mobile = user.Mobile;
            existingUser.Password = user.Password;

            return existingUser;
        }

        void IUserDetailsRepository.Remove(UserDetails user)
        {
            ((IUserDetailsRepository)this).Remove(user.Id);
        }

        void IUserDetailsRepository.Remove(long userId)
        {
            var existingUser = ((IUserDetailsRepository)this).Find(userId);
            if (existingUser == null)
            {
                throw new InvalidOperationException($"User with id {userId} does not exist in database");
            }

            Values.Remove(existingUser);
        }

        UserDetails IUserDetailsRepository.Find(long userId)
        {
            return Values.FirstOrDefault(x => x.Id == userId);
        }

        UserDetails IUserDetailsRepository.Find(Expression<Func<UserDetails, bool>> filter)
        {
            var filterFunction = filter.Compile();
            return Values.FirstOrDefault(filterFunction);
        }

        IEnumerable<UserDetails> IUserDetailsRepository.GetAll()
        {
            return Values.ToList();
        }

        IEnumerable<UserDetails> IUserDetailsRepository.FindAll(Expression<Func<UserDetails, bool>> filter)
        {
            var filterFunction = filter.Compile();
            return Values.Where(filterFunction).ToList();
        }
    }
}
