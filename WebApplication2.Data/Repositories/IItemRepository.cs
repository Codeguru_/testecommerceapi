﻿using System;
using System.Collections.Generic;
using WebApplication2.Core.Models;

namespace WebApplication2.Data.Repositories
{
    public interface IItemsRepository
    {
        Item Find(object itemId);
        IEnumerable<Item> GetAll();
        void Remove(object itemId);
        Item Add(Item item);
        void Remove(Item item);
    }
}
