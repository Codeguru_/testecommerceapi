﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace WebApplication2.Data.Repositories
{
    public abstract class JsonFileRepository<T> where T : class
    {
        private readonly string _databasePath;
        private IList<T> _values;
        protected IList<T> Values => _values ?? (_values = LoadData());
        private readonly object _locker = new object();

        protected JsonFileRepository(string databasePath)
        {
            this._databasePath = databasePath;
        }

        public virtual List<T> LoadData()
        {
            lock (_locker)
            {
                var json = File.ReadAllText(_databasePath);
                return JsonConvert.DeserializeObject<List<T>>(json, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Formatting = Formatting.Indented,
                });
            }
        }

        public virtual void SaveChanges()
        {
            lock (_locker)
            {
                var json = JsonConvert.SerializeObject(_values, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Formatting = Formatting.Indented,
                });
                File.WriteAllText(_databasePath, json);
            }
        }
    }
}
