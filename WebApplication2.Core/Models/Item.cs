﻿using System;

namespace WebApplication2.Core.Models
{
    public class Item
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price  { get; set; }
    }
}
