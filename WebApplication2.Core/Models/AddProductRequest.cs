﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApplication2.Core.Models
{
    public class AddProductRequest
    {
        [Required]
        public Guid ProductId { get; set; }
        [Range(1, 999999)]
        public int Quantity { get; set; }
    }
}
