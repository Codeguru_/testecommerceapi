﻿using System;
using Newtonsoft.Json;
using WebApplication2.Core.Extensions;

namespace WebApplication2.Core.Models
{
    public class AuthToken
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; }
        public string RequestOrigin { get; }
        public long UserId { get; }
        public long UnixTimeStamp { get; }

        public AuthToken(string accessToken, long userId, string requestOrigin, long unixTimeStamp)
        {
            SimpleTypesExtensions.ThrowIfDefaultValues(
                new object[] { accessToken, userId, requestOrigin, unixTimeStamp }, 
                p => throw new ArgumentException($"A value of {p} is not a valid value for parameter of type {p.GetType().Name}"));

            this.AccessToken = accessToken;
            this.UserId = userId;
            this.RequestOrigin = requestOrigin;
            this.UnixTimeStamp = unixTimeStamp;
        }

        public static bool operator ==(AuthToken token1, AuthToken token2)
        {
            if (object.ReferenceEquals(token1, token2))
            {
                return true;
            }
            if (object.ReferenceEquals(token1, null) ||
                object.ReferenceEquals(token2, null))
            {
                return false;
            }

            return
                token1.AccessToken.Equals(token2.AccessToken, StringComparison.InvariantCultureIgnoreCase)
                && token1.UserId == token2.UserId
                && token1.UnixTimeStamp == token2.UnixTimeStamp;
        }

        public bool Equals(AuthToken other)
        {
            return this == other;
        }

        public static bool operator !=(AuthToken token1, AuthToken token2)
        {
            return !(token1 == token2);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            var objAsAuth = obj as AuthToken;
            if (objAsAuth != null)
            {
                return this == objAsAuth;
            }

            return false;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return 17 * 23 + AccessToken.GetHashCode();
            }       
        }
    }
}
