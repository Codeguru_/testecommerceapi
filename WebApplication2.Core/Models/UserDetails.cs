﻿namespace WebApplication2.Core.Models
{
    public class UserDetails
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Mobile { get; set; }
        public long DateRegisteredTimeStamp { get; set; }
        public string SecurityStamp { get; set; }
    }
}