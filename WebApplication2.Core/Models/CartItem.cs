﻿using System;

namespace WebApplication2.Core.Models
{
    public class CartItem
    {
        public long ItemId { get; }
        public long CartId { get; }
        public int Quantity { get; set; }

        private CartItem()
        {
        }

        public CartItem(long cartId, long itemId, int quantity = 1)
        {
            ItemId = itemId;
            CartId = cartId;
            Quantity = quantity;
        }
    }
}
