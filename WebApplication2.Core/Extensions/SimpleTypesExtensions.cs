﻿using System;
using System.Linq;

namespace WebApplication2.Core.Extensions
{
    public static class SimpleTypesExtensions
    {
        public static long ToUnixTimeStamp(this DateTime date)
        {
            return Convert.ToInt64((date - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds);
        }

        public static DateTime ToDateTime(this DateTime date, long unixTimeStamp)
        {
            return ToDateTime(unixTimeStamp);
        }

        public static DateTime ToDateTime(long unixTimeStamp)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(unixTimeStamp);
        }

        public static void ThrowIfDefaultValues(object[] parameters, Action<object> actionOnError)
        {
            if (parameters != null && parameters.Any())
            {
                foreach (var parameter in parameters)
                {
                    if (parameter == null)
                    {
                        actionOnError(parameter);
                    }

                    var type = parameter.GetType();
                    if (type.IsValueType && Activator.CreateInstance(type).Equals(parameter))
                    {
                        actionOnError(parameter);
                    }
                }
            }
        }
    }
}
