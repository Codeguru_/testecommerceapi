﻿using System;
using WebApplication2.Core.Models;

namespace WebApplication2.Core.Representations
{
    public class CartItemRepresentation
    {
        public long Id { get; set; }
        public long ItemId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; } = 1;

        public static implicit operator CartItemRepresentation(Item from)
        {
            return new CartItemRepresentation
            {
                ItemId = from.Id,
                Name = from.Name,
                Description = from.Description,
                Price = from.Price,
            };
        }

        public static implicit operator CartItemRepresentation(CartItem from)
        {
            return new CartItemRepresentation
            {
                Id = from.ItemId,
                ItemId = from.ItemId,
                Quantity = from.Quantity
            };
        }

        public static implicit operator Item(CartItemRepresentation from)
        {
            return new Item
            {
                Id = from.ItemId,
                Name = from.Name,
                Description = from.Description,
                Price = from.Price,
            };
        }
    }
}
