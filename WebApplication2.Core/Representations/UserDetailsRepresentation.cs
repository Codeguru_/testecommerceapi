﻿using System;
using System.ComponentModel.DataAnnotations;
using WebApplication2.Core.Models;

namespace WebApplication2.Core.Representations
{
    public class UserDetailsRepresentation
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [DataType((DataType.Password))]
        public string Password { get; set; }

        [Required]
        [DataType((DataType.Password))]
        public string ConfirmPassword { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"\d{3}-\d{7}")]
        public string Mobile { get; set; }

        public string SecurityStamp { get; set; }

        public static implicit operator UserDetails(UserDetailsRepresentation from)
        {
            return new UserDetails
            {
                Email = from.Email,
                Mobile = from.Mobile,
                Password = from.Password,
            };
        }

        public static implicit operator UserDetailsRepresentation(UserDetails from)
        {
            return new UserDetailsRepresentation
            {
                Email = from.Email,
                Mobile = from.Mobile,
                Password = from.Password,
            };
        }
    }
}
