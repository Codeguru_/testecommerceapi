﻿using System;
using System.Web.Http;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;

namespace WebApplication2.Identity
{
    public class SimpleInjectorConfig
    {
        public static Container Configure()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            new Data.SimpleInjector.DataSimpleInjectorPackage().RegisterServices(container);
            new Domain.SimpleInjector.DomainSimpleInjectorPackage().RegisterServices(container);
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
            container.Verify();
            return container;
        }
    }
}