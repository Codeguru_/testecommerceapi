﻿using System;
using System.Web;
using System.Web.Http;
using WebApplication2.Data.SimpleInjector;

namespace WebApplication2.Identity
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            DataSimpleInjectorPackage.AdminDatabasePath = HttpContext.Current.Server.MapPath("~/App_Data/db.json");
            GlobalConfiguration.Configure(WebApiConfig.Register);
            SimpleInjectorConfig.Configure();
        }
    }
}
