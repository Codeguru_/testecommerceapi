﻿using System;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication2.Core.Representations;
using WebApplication2.Domain.Exceptions;
using WebApplication2.Domain.Services;

namespace WebApplication2.Identity.Controllers
{
    [RoutePrefix("account")]
    public class AccountController : ApiController
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            this._accountService = accountService;
        }


        [HttpPost]
        [Route("register")]
        [ResponseType(typeof(void))]
        public IHttpActionResult Post(UserDetailsRepresentation userDetails)
        {
            try
            {
                _accountService.Register(userDetails);                
            }
            catch (BadRequestException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok();
        }        
    }
}
