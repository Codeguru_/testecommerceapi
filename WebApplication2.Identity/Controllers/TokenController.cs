﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication2.Core.Models;
using WebApplication2.Domain.Exceptions;
using WebApplication2.Domain.Services;

namespace WebApplication2.Identity.Controllers
{
    [AllowAnonymous]
    public class TokenController : ApiController
    {
        private readonly IAuthenticationService _authenticationService;

        public TokenController(IAuthenticationService authenticationService)
        {
            this._authenticationService = authenticationService;
        }

        /// <summary>
        /// Gets user access token
        /// </summary>
        /// <param name="loginData">User login data</param>
        /// <returns>User access token or bad request</returns>
        [ResponseType(typeof(AuthToken))]
        public IHttpActionResult Post(UserDetails loginData)
        {
            if (Request.IsLocal())
            {
                return Ok(_authenticationService.Login(loginData, "local"));
            }

            Request.Headers.TryGetValues("Origin", out var origin);
            return Ok(_authenticationService.Login(loginData, origin.FirstOrDefault() ?? throw new BadRequestException("Origin header is required")));
        }
    }
}