﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace WebApplication2.Api.Api.Models
{
    public class ErrorResult<TException> : IHttpActionResult where TException : Exception
    {
        private readonly HttpRequestMessage _requestMessage;
        private readonly HttpStatusCode _statusCode;
        private readonly TException _exception;

        public ErrorResult(HttpRequestMessage requestMessage, HttpStatusCode statusCode, TException exception)
        {
            this._requestMessage = requestMessage;
            this._statusCode = statusCode;
            this._exception = exception;
        }

        Task<HttpResponseMessage> IHttpActionResult.ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(Execute());
        }

        private HttpResponseMessage Execute()
        {
            return _requestMessage.CreateErrorResponse(_statusCode, _exception?.InnerException ?? _exception);
        }
    }
}