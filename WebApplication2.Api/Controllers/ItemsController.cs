﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication2.Core.Models;
using WebApplication2.Data.Repositories;

namespace WebApplication2.Api.Controllers
{
    //[WebbAppAuthorize]
    public class ItemsController : ApiController
    {
        private readonly IItemsRepository _stubRepository;

        public ItemsController(IItemsRepository stubRepository)
        {
            this._stubRepository = stubRepository;
        }

        [HttpGet]
        [Route("api/items")]
        [ResponseType(typeof(IEnumerable<Item>))]
        public IHttpActionResult Get()
        {
            return Ok(_stubRepository.GetAll());
        }
    }
}
