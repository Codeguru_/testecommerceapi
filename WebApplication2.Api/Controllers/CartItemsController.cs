﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication2.Api.Filters;
using WebApplication2.Core.Models;
using WebApplication2.Core.Representations;
using WebApplication2.Domain.Services;

namespace WebApplication2.Api.Controllers
{
    [WebbAppAuthorize]
    [RoutePrefix("api/users/{userId:long}/cart/items")]
    public class CartItemsController : ApiController
    {
        private readonly ICartItemsService _itemsService;

        public CartItemsController(ICartItemsService itemsService)
        {
            this._itemsService = itemsService;
        }

        [Route("")]
        [ResponseType(typeof(CartItemRepresentation))]
        public IHttpActionResult Post([FromUri]long userId, [FromBody]CartItemRepresentation model)
        {
            if (model == null)
            {
                return BadRequest();
            }
            return Ok(_itemsService.AddOrUpdate(userId, model.ItemId, model.Quantity));
        }

        [Route("")]
        [ResponseType(typeof(IEnumerable<CartItemRepresentation>))]
        public IHttpActionResult Get(long userId)
        {
            return Ok(_itemsService.GetAll(userId));
        }

        [Route("{itemId:guid}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult Delete(long userId, CartItem item)
        {
            if (item == null)
            {
                _itemsService.RemoveAll(userId);
            }
            else
            {
                _itemsService.Remove(userId, item.ItemId);
            }
            return Ok();
        }
    }
}
