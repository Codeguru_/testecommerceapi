﻿using System;
using System.Web.Http;
using System.Web.Http.Description;
using WebApplication2.Api.Filters;
using WebApplication2.Domain.Services;

namespace WebApplication2.Api.Controllers
{
    [WebbAppAuthorize]
    [RoutePrefix("api/users/{userId:long}/cart")]
    public class CartController : ApiController
    {
        private readonly ICartItemsService _itemsService;

        public CartController(ICartItemsService itemsService)
        {
            this._itemsService = itemsService;
        }

        [HttpDelete]
        [Route("")]
        [ResponseType(typeof(void))]
        public IHttpActionResult Delete(long userId)
        {
            _itemsService.RemoveAll(userId);
            return Ok();
        }
    }
}
