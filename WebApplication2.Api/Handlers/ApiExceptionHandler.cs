﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;
using WebApplication2.Domain.Exceptions;

namespace WebApplication2.Api.Handlers
{
    public class ApiExceptionHandler : IExceptionHandler
    {
        public void Handle(ExceptionHandlerContext context)
        {
            switch (context.Exception)
            {
                case ForbiddenException _:
                {
                    context.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Unauthorized");
                    break;
                }
                case NotFoundException _:
                {
                    context.Result = new NotFoundResult(context.Request);
                    break;
                }
                case BadRequestException _:
                {
                    context.Result = new BadRequestResult(context.Request);
                    break;
                }
                case HttpException ex:
                {
                    context.Request.CreateErrorResponse((HttpStatusCode)ex.GetHttpCode(), ex.Message);
                    break;
                }
                case Exception ex:
                {
                    context.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                    break;
                }
            }
        }

        public Task HandleAsync(ExceptionHandlerContext context, CancellationToken cancellationToken)
        {
            return Task.Run(() => Handle(context), cancellationToken);
        }
    }
}