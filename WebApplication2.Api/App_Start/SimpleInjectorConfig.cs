﻿using System;
using System.Web.Http;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;

namespace WebApplication2.Api
{
    public class SimpleInjectorConfig
    {
        public static Container Configure()
        {            
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            new Data.SimpleInjector.DataSimpleInjectorPackage().RegisterServices(container);
            new Domain.SimpleInjector.DomainSimpleInjectorPackage().RegisterServices(container);
            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
            container.Verify();
            return container;
        }
    }
}