﻿using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Controllers;
using WebApplication2.Domain.Exceptions;
using WebApplication2.Domain.Services;
using System.Collections.Generic;
using System.Net.Http;

namespace WebApplication2.Api.Filters
{
    public class WebbAppAuthorizeAttribute : AuthorizeAttribute
    {
        private IAuthenticationService _authenticationService;
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            _authenticationService = 
                _authenticationService
                ?? (IAuthenticationService)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IAuthenticationService));
            actionContext.ActionArguments.TryGetValue("userId", out var actionUserId);
            if (actionUserId == null)
            {
                actionContext.Request.GetRouteData().Values.TryGetValue("userId", out actionUserId);
            }
            bool withUserId = !string.IsNullOrWhiteSpace(actionUserId?.ToString());
            actionContext.Request.Headers.TryGetValues("Authorization", out var authHeaders);
            string authHeader = authHeaders?.FirstOrDefault();
            var authValues = new Dictionary<string, string>();
            if (string.IsNullOrWhiteSpace(authHeader))
            {
                throw new ForbiddenException("Authorization denied");
            }
            long convertedTimeStamp;
            try
            {
                actionContext.Request.Headers.TryGetValues("Origin", out var headersOrigin);
                authValues.Add("ORIGIN", headersOrigin?.FirstOrDefault());
                string[] splittedAuthenticationHeader = authHeader.Replace("WebApplication2HMAC: '", "").TrimEnd('\'').Split(';');
                foreach (var str in splittedAuthenticationHeader)
                {
                    var pair = str.Split('=');
                    if (string.IsNullOrWhiteSpace(pair[0]))
                    {
                        throw new BadRequestException("Invalid authentication data");
                    }
                    authValues.Add(pair[0].ToUpperInvariant(), pair[1]);                    
                }
                convertedTimeStamp = Convert.ToInt64(authValues["TIMESTAMP"]);
            }
            catch(Exception ex)
            {
                throw new ForbiddenException("Authorization denied");
            }

            if (actionUserId != null)
            {
                try
                {
                    actionUserId = Convert.ToInt64(actionUserId);
                }
                catch
                {
                    throw new BadRequestException(
                        $"User id must be a number. The value {actionUserId} is not supported");
                }
            }

            var origin = actionContext.Request.IsLocal() ? "local" : authValues["ORIGIN"];

            try
            {
                if (!_authenticationService.CheckToken(
                    Convert.ToInt64(withUserId? actionUserId : authValues["USERID"]),
                    origin,
                    convertedTimeStamp,
                    authValues["ACCESS_TOKEN"]))
                {
                    throw new ForbiddenException("Authorization denied");
                }
            }
            catch
            {
                throw new BadRequestException("Invalid authentication token format");
            }
        }
    }
}
