﻿function Collection() {
}

Collection.prototype = Object.create(Array.prototype);
Collection.prototype.remove = function (item) {
    var index = this.indexOf(item);
    if (index < 0) {
        return new Error('Element not found');
    }
    this.splice(index, 1);
}
Collection.prototype.add = function (item) {
    this.push(item);
}
Collection.prototype.clear = function () {
    this.length = 0;
}
Collection.prototype.firstOrDefault = function (filter) {
    if (typeof filter !== 'function') {
        return Error('filter must be a boolean-valued function');
    }

    this.forEach(el => {
        if (filter(el) === true) {
            return el;
        }
        return null;
    });
}
Collection.prototype.where = function (filter) {
    if (typeof filter !== 'function') {
        return Error('filter must be a boolean-valued function');
    }

    var result = [];
    this.forEach(el => {
        if (filter(el) === true) {
            result.push(el);
        }
    });

    return result;
}
Collection.prototype.where = function () {
    this.length = 0;
}

Collection.prototype.replaceData = function(data) {
    if (data && Array.isArray(data)) {
        var self = this;
        self.length = 0;
        data.forEach(el => {
            self.push(el);
        });
    }
}