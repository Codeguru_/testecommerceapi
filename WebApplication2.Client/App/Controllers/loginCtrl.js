﻿(function() {
    angular.module('webApplication2')
        .controller('LoginCtrl', ['loginService', 'messageService', 'pageService', 'userDetails', LoginCtrl]);

    function LoginCtrl(loginService, messageService, pageService, userDetails) {        
        var vm = this;
        vm.message = null;
        vm.creds = userDetails.getDetails();
        vm.shouldShowCart = false;

        vm.userLoggedIn = function () {
            return vm.creds.isLoggedIn;
        }

        vm.show = messageService(vm);
        vm.pageService = pageService;

        vm.login = function () {
            if (vm.creds.email && vm.creds.password) {

                loginService.login(vm.creds, function (data) {                
                    vm.message = '';
                    vm.creds.password = '';
                    userDetails.setDetails(data);
                    localStorage.setItem('authTokenData', JSON.stringify(userDetails.getDetails()));
                    vm.pageService.setCurrentPage('products');
                },function (response) {                              
                    if (response.data.exceptionMessage) {
                        vm.show(response.data.exceptionMessage + '\r\n');
                    }
                    else
                    if (response.data.error) {
                        vm.show(response.data.error + '\r\n');
                    }
                    else
                    if (response.data.error_description) {
                        vm.show(response.data.error_description + '\r\n');
                    } else {
                        if (response.status === 404) {
                            vm.show('User not found' + '\r\n');
                        } else if (response.status === 400) {
                            vm.show('Incorrect username or password' + '\r\n');
                        }
                    }

                    vm.userLoggedIn = false;
                    vm.creds.password = '';   
                    userDetails.setDetails(vm.creds);
                });
            }
        }

        vm.register = function () {
            vm.creds.confirmPassword = vm.creds.password;
            userDetails.setDetails(vm.creds);

            if (vm.creds.email && vm.creds.password) {
                loginService.register(vm.creds, function (data) {
                    vm.show('User is registered successfully!');
                    vm.userLoggedIn = true;
                    vm.login();
                }, function (response) {
                    vm.userLoggedIn = false;
                    vm.show(response.statusText + '\r\n');

                    if (response.data.exceptionMessage) {
                        for (var key in response.data.modelState) {
                            vm.show(response.data.modelState[key] + '\r\n');
                        };
                    }
                });
            }
        }

        vm.logout = function () {
            userDetails.clear();
            localStorage.removeItem('authTokenData');
            vm.pageService.setCurrentPage('login');
        }

        vm.localStorageCreds = JSON.parse(localStorage.getItem('authTokenData'));

        if (vm.localStorageCreds && vm.localStorageCreds.access_token) {
            userDetails.setDetails(vm.localStorageCreds);
            vm.pageService.setCurrentPage('products');
        }
    }
}())