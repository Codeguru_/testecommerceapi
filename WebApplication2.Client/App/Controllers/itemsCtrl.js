﻿(function () {
    "use strict";
    angular
        .module('webApplication2')
        .controller('ItemsCtrl', ['productService', 'messageService', 'userDetails', 'cartResource', 'cartService', 'pageService', ItemsCtrl]);

    function ItemsCtrl(productService, messageService, userDetails, cartResource, cartService, pageService) {
        var vm = this;
        vm.editMode = false;
        vm.product = {};
        vm.products = [];
        vm.productService = productService;
        vm.cartService = cartService;
        vm.show = messageService(vm);
        vm.message = null;
        vm.creds = userDetails.getDetails();
        vm.pageService = pageService;

        vm.submit = function (editForm) {
            if (vm.product.id && vm.product.id > 0) {
                vm.product.$update(function (data) {
                    vm.show('product saved');
                    vm.updateProductsList();
                }, function (response) {
                    vm.show(response.statusText + '\r\n');
                });
            }
            else {
                vm.product.$save(function (data) {
                    vm.show('product added');
                }, function (response) {
                    vm.show(response.statusText + '\r\n');
                });
            }

            editForm.$setPristine();
            vm.editMode = false;
        };

        vm.productClicked = function (product) {            
        };

        vm.addNewItem = function () {
            vm.product = vm.newProduct(vm.product);
            vm.editMode = true;
            vm.message = null;
        };

        vm.removeItems = function () {
            for (var i in vm.selectedItems) {
                var index = vm.products.indexOf(vm.selectedItems[i]);
                if (index > -1) {
                    vm.products.splice(index, 1);
                }
            }
            vm.selectedItems = [];
        };

        vm.newProduct = function (item) {
            var newItem = new productService();
            return newItem;
        };

        vm.tryUpdateProductsList = function () {
            productService.get(function(data) {
                if (data) {
                    data.forEach(function (item) {
                        var newCartItem = new cartResource();
                        newCartItem.itemId = item.id;
                        newCartItem.name = item.name;
                        newCartItem.description = item.description;
                        newCartItem.price = item.price;
                        newCartItem.quantity = item.quantity || 1;
                        vm.products.push(newCartItem);
                    });
                }},
                function(response) {
                    vm.show(response.statusText + '\r\n');
                });
        };

        vm.addToCart = function (product) {
            vm.cartService.addToCart(product);
        }

        vm.updateProductsList = function () {
            throw new Error("Not implemented");
        };

        userDetails.onDetailsChanged(function (newData) {
            vm.tryUpdateProductsList();
        });

        vm.tryUpdateProductsList();
    }
}());