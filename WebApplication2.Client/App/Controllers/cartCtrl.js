﻿(function () {
    "use strict";
    angular
        .module('webApplication2')
        .controller('CartCtrl', ['cartService', 'messageService', 'userDetails', 'pageService', CartCtrl]);

    function CartCtrl(cartService, messageService, userDetails, pageService) {
        var vm = this;        
        vm.product = {};        
        vm.show = messageService(vm);
        vm.creds = userDetails.getDetails();
        vm.message = null;
        vm.pageService = pageService;

        vm.plusClicked = function (item) {
            cartService.incQnt(item);
        }

        vm.minusClicked = function (item) {
            cartService.decQnt(item);            
        }

        vm.submit = function (editForm) {
            if (vm.product.id && vm.product.id > 0) {
                vm.product.$update(function (data) {
                    vm.show('product saved');
                    vm.tryUpdateProductsList();
                }, function (response) {
                    vm.show(response.statusText + '\r\n');
                });
            }
            else {
                vm.product.$save(function (data) {
                    vm.show('product added');
                }, function (response) {
                    vm.show(response.statusText + '\r\n');
                });
            }
        };

        vm.tryUpdateProductsList = function () {
            cartService.updateProductsList(vm.creds.userId);
        }

        vm.updateTotalSum = function () {
            return cartService.totalSum;
        }

        vm.getTotalSum = function() {
            return cartService.totalSum;
        }

        vm.getProducts = function () {
            return cartService.products;
        }

        cartService.updateProducList();
    }
}());