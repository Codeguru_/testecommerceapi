﻿(function () {
    "use strict";
    angular.module("commonService", ["ngResource"]).constant("appSettings",
    {
        baseurl: "http://localhost:52055/",
        tokenServerUrl: "http://localhost:49878/"
    });
}())