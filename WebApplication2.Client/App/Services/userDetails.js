﻿(function() {
    "use strict";

    angular
        .module('commonService')
        .factory('userDetails', userDetails);

    function userDetails() {
        var self = this;
        self.detailsChanged = [];
        var profile = {};

        var clear = function() {
            profile = {
                userId: null,
                email: null,
                password: null,
                confirmPassword: null,
                access_token: null,
                isLoggedIn: false
            };
        }

        clear();

        var getDetails = function() {
            return profile;
        }

        var setDetails = function(newDetails) {
            if (newDetails.userId) profile.userId = newDetails.userId;
            if (newDetails.email) profile.email = newDetails.email;
            if (newDetails.password) profile.password = newDetails.password;
            if (newDetails.confirmPassword) profile.confirmPassword = newDetails.confirmPassword;
            if (newDetails.unixTimeStamp) profile.timeStamp = newDetails.unixTimeStamp;
            if (newDetails.access_token) {
                profile.access_token = newDetails.access_token;
                profile.isLoggedIn = true;
            }
            if (self.detailsChanged) {
                self.detailsChanged.forEach(function (handler) {
                    handler(newDetails);
                });
            }
        }

        return {
            getDetails: getDetails,
            setDetails: setDetails,
            clear: clear,
            onDetailsChanged: function (newHandler) {
                if (newHandler && typeof newHandler === "function") {
                    self.detailsChanged.push(newHandler);
                }
            }
        }
    }
}());