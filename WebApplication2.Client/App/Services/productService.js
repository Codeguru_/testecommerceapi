﻿(function () {
    'use strict';
    angular
        .module('commonService')
        .factory('productService', ['$resource', 'appSettings', 'userDetails', productService]);

    function productService($resource, appSettings, userDetails) {
        var self = this;
        self.appSettings = appSettings;
        self.userDetails = userDetails;
        self.getAuthHeader = function () {
            return 'WebApplication2HMAC: \'' +
                'access_token=' + self.userDetails.getDetails().access_token + ';' +
                'timeStamp=' + self.userDetails.getDetails().timeStamp + ';' +
                'userId=' + self.userDetails.getDetails().userId + '\'';
        };
        
        return $resource(appSettings.baseurl + 'api/items', null,
            {
                'query': {
                    cache: true,
                    method: 'GET',
                    isArray: true,
                    headers: { 'Authorization': function () { return self.getAuthHeader(); } },                    
                },
                'get': {
                    cache: true,
                    method: 'GET',
                    isArray: true,
                    headers: { 'Authorization': function () { return self.getAuthHeader(); } }
                }
            });
    }
}());