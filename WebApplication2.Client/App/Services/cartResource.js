﻿(function () {
    'use strict';
    angular
        .module('commonService')
        .factory('cartResource', ['$resource', 'appSettings', 'userDetails', cartResource]);

    function cartResource($resource, appSettings, userDetails) {
        var self = this;        
        self.appSettings = appSettings;
        self.userDetails = userDetails;
        self.getAuthHeader = function () {
            return 'WebApplication2HMAC: \'' +
                'access_token=' + self.userDetails.getDetails().access_token + ';' +
                'timeStamp=' + self.userDetails.getDetails().timeStamp + ';' +
                'userId=' + self.userDetails.getDetails().userId +'\'';
        };

        return $resource(appSettings.baseurl + 'api/users/:userId/cart/items', null,
            {
                'query': {
                    method: 'GET',
                    cache: false,
                    isArray: true,
                    headers: { 'Authorization': function () { return self.getAuthHeader(); } },
                },
                'get': {
                    method: 'GET',
                    cache: false,
                    isArray: true,
                    headers: { 'Authorization': function () { return self.getAuthHeader(); } },
                },
                'save': {                    
                    method: 'POST',
                    cache: false,
                    headers: { 'Authorization': function () { return self.getAuthHeader(); } },
                },
                'update': {                    
                    method: 'PUT',
                    cache: false,
                    headers: { 'Authorization': function () { return self.getAuthHeader(); } },
                },
                'delete': {                    
                    method: 'DELETE',
                    cache: false,
                    headers: { 'Authorization': function () { return self.getAuthHeader(); } },
                }
            });
    }
}());