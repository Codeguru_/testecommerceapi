﻿(function () {
    'use strict';
    angular
        .module('commonService')
        .factory('pageService', pageService);

    function pageService() {
        var self = {};
        self.currectPage = 'login';

        self.onPageChangeHandlers = [];

        self.getCurrentPage = function() {
            return self.currectPage;
        }

        self.previousPage = '';

        self.setCurrentPage = function(newValue) {
            if (typeof newValue === 'string') {
                self.previousPage = self.currectPage;
                self.currectPage = newValue;
                if (self.onPageChangeHandlers) {
                    self.onPageChangeHandlers.forEach(function (onPageChangeHandler) {
                        onPageChangeHandler(newValue);
                    });
                }

            } else {
                throw new Error('Incorrect format for parameter: newValue');
            }
        }

        return {
            getCurrentPage: self.getCurrentPage,
            setCurrentPage: self.setCurrentPage,
            currentPageName: self.currectPage,
            onPageChange: function(newHandler) {
                if (newHandler && typeof newHandler === 'function') {
                    self.onPageChangeHandlers.push(newHandler);
                }
            },
            goBack: function() {
                self.setCurrentPage(self.previousPage);
            }
        }
    }
}());