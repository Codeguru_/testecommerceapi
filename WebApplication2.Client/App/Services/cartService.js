﻿(function() {
    'use strict';
    angular
        .module('commonService')
        .factory('cartService', ['cartResource', 'messageService', 'userDetails', 'pageService', cartService]);

    function cartService(cartResource, messageService, userDetails, pageService) {
        var self = {};
        self.totalSum = 0;
        self.products = [];
        self.creds = userDetails.getDetails();
        self.message = null;
        self.products = new Collection();
        self.show = messageService(self);

        self.updateProducList = function(userId) {
            cartResource.get({ 'userId': userId || self.creds.userId }, function (data) {
                self.products.replaceData(data);
                self.updateTotalSum();
            }, function (response) {
                self.show('Session expired or user not logged in',
                    function () {
                        localStorage.removeItem('authTokenData');
                        pageService.setCurrentPage('login');
                    });
            });
        }

        self.addToCart = function (product, userId) {
            product.$save({'userId': userId || self.creds.userId },
                function(data) {
                    self.show('Added to Cart');
                },
                function(response) {
                    self.show('Session expired or user not logged in',
                        function() {
                            localStorage.removeItem('authTokenData');
                            pageService.setCurrentPage('login');
                        });
                });
        }

        self.incQnt = function (product) {
            product.quantity++;
            self.product = product;
            self.totalSum += product.price;
            updateProductOnServer();
        }

        self.decQnt = function (product) {
            product.quantity--;
            self.product = product;
            self.totalSum -= product.price;
            updateProductOnServer();
        }

        function updateProductOnServer() {
            self.product.$save({'userId': self.creds.userId}, function (data) {                
                if (self.product.quantity < 1) {
                    self.products.remove(self.product);
                }
            }, function (response) {
                self.show(response.statusText + '\r\n');
            });
        }

        self.updateTotalSum = function () {
            self.totalSum = 0;
            if (self.products.length > 0) {
                self.products.forEach(p => {
                    self.totalSum += p.price * p.quantity;
                });
            }
        }

        pageService.onPageChange(function (newPage) {
            if (newPage === 'cart') {
                self.updateProducList();
            }
        });

        return self;
    }
}());