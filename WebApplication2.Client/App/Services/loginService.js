﻿(function () {
    'use strict';
    angular
        .module('commonService')
        .factory('loginService', ['$resource', 'appSettings', loginService]);

    function loginService($resource, appSettings) {
        return $resource(appSettings.baseurl + 'api/', null,
            {
                'register': {
                    method: 'POST',
                    url: appSettings.tokenServerUrl + 'account/register'
                },
                'login': {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    url: appSettings.tokenServerUrl + 'token',
                    transformRequest: function (data, headersGetter) {
                        var str = [];
                        for (var d in data) {
                            if (data.hasOwnProperty(d)) {
                                str.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
                            }
                        }
                        return str.join('&');
                    }
                },
                'logout': {
                    method: 'POST',
                    url: appSettings.baseurl + 'api/account/logout'
                }
            });
    }
}());