﻿(function () {
    "use strict";
    angular
        .module("webApplication2")
        .factory("messageService", ["$timeout", messageService]);

    function messageService($timeout) {
        var self = this;
        self.$timeout = $timeout;

        return function (obj) {
            self.$obj = obj;

            return function (message, callback, timeoutMs) {
                if (!timeoutMs || timeoutMs < 1) {
                    timeoutMs = 2000;
                }
                self.$obj.message = message;
                self.callback = callback;
                self.$timeout(function() {
                    self.$obj.message = null; 
                    if (self.callback && typeof self.callback === 'function') {
                        callback();
                    }
                }, timeoutMs);
            };
        };
    }
}());