﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebApplication2.Core.Models;
using WebApplication2.Core.Representations;
using WebApplication2.Data.Repositories;

namespace WebApplication2.Domain.Services
{
    public class CartItemsService : ICartItemsService
    {
        private readonly ICartItemsRepository _cartItemsRepository;
        private readonly IItemsRepository _itemsRepository;

        public CartItemsService(ICartItemsRepository cartItemsRepository, IItemsRepository itemsRepository)
        {
            this._cartItemsRepository = cartItemsRepository;
            this._itemsRepository = itemsRepository;
        }

        CartItemRepresentation ICartItemsService.AddOrUpdate(long userCartId, long itemId, int quantity)
        {
            var cartItem = _cartItemsRepository.AddOrUpdate(userCartId, itemId, quantity);
            var cartItemRepresentation = (CartItemRepresentation)_itemsRepository.Find(itemId);
            cartItemRepresentation.Id = cartItem.ItemId;
            cartItemRepresentation.Quantity = cartItem.Quantity;
            return cartItemRepresentation;
        }

        IEnumerable<CartItemRepresentation> ICartItemsService.GetAll(long userCartId)
        {
            var dbItemsDictionary = _itemsRepository.GetAll().ToDictionary(x => x.Id);
            return _cartItemsRepository.GetAll(userCartId).Where(x => x.Quantity > 0).Select(x => ConvertToRepresentation(x, dbItemsDictionary[x.ItemId])).ToList();
        }

        void ICartItemsService.Remove(long userCartId, long itemId)
        {
            _cartItemsRepository.Remove(userCartId, itemId);
        }

        void ICartItemsService.RemoveAll(long userCartId)
        {
            _cartItemsRepository.RemoveAll(userCartId);
        }

        private static CartItemRepresentation ConvertToRepresentation(CartItem cartItem, Item item)
        {
            var cartItemRepresentation = (CartItemRepresentation)item;
            cartItemRepresentation.Id = cartItem.CartId;
            cartItemRepresentation.Quantity = cartItem.Quantity;
            return cartItemRepresentation;
        }
    }
}
