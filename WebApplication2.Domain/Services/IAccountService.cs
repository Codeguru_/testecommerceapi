﻿using System;
using WebApplication2.Core.Representations;

namespace WebApplication2.Domain.Services
{
    public interface IAccountService
    {
        void Register(UserDetailsRepresentation user);
    }
}