﻿using System;
using DevOne.Security.Cryptography.BCrypt;
using WebApplication2.Core.Representations;
using WebApplication2.Data.Repositories;
using WebApplication2.Domain.Exceptions;

namespace WebApplication2.Domain.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUserDetailsRepository _userRepository;

        public AccountService(IUserDetailsRepository userRepository)
        {
            this._userRepository = userRepository;
        }

        void IAccountService.Register(UserDetailsRepresentation user)
        {
            try
            {
                user.Password = BCryptHelper.HashPassword(user.Password, BCryptHelper.GenerateSalt());
                _userRepository.Add(user);
            }
            catch (InvalidOperationException ex)
            {
                throw new BadRequestException(ex.Message);
            }
        }
    }
}
