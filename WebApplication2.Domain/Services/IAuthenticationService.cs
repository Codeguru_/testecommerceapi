﻿using System;
using WebApplication2.Core.Models;
using WebApplication2.Core.Representations;

namespace WebApplication2.Domain.Services
{
    public interface IAuthenticationService
    {
        AuthToken GenerateToken(long userId, string requestOrigin, long expiresTimeStamp);
        bool CheckToken(long userId, string requestOrigin, long expiresTimeStamp, string token);
        bool CheckToken(AuthToken authTokenObject);
        AuthToken Login(UserDetailsRepresentation loginData, string requestOrigin);
        AuthToken Login(UserDetailsRepresentation loginData, string requestOrigin, int expireMinutes);
    }
}