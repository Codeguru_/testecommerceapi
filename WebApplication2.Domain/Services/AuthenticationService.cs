﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using DevOne.Security.Cryptography.BCrypt;
using WebApplication2.Core.Extensions;
using WebApplication2.Core.Models;
using WebApplication2.Core.Representations;
using WebApplication2.Data.Repositories;
using WebApplication2.Domain.Exceptions;

namespace WebApplication2.Domain.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private const string Alg = "HmacSHA256";
        private readonly IUserDetailsRepository _userRepository;

        public AuthenticationService(IUserDetailsRepository userRepository)
        {
            _userRepository = userRepository;
        }

        AuthToken IAuthenticationService.GenerateToken(long userId, string requestOrigin, long expiresTimeStamp)
        {
            var hash = string.Concat(
                ":",
                new[]
                {
                    requestOrigin,
                    userId.ToString(),
                    expiresTimeStamp.ToString()
                });
            string hashLeft;
            string hashRight;

            using (var hmac = HMAC.Create(Alg))
            {
                hmac.Key = Encoding.UTF8.GetBytes(userId.ToString());
                hmac.ComputeHash(Encoding.UTF8.GetBytes(hash));
                hashLeft = Convert.ToBase64String(hmac.Hash);
                hashRight = string.Join(":", userId, expiresTimeStamp.ToString());
            }

            return new AuthToken(
                Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Join(":", hashLeft, hashRight))),
                userId,
                requestOrigin,
                expiresTimeStamp);
        }

        bool IAuthenticationService.CheckToken(long userId, string requestOrigin, long expiresTimeStamp, string token)
        {
            string[] parts = Encoding.UTF8.GetString(Convert.FromBase64String(token)).Split(':');

            return
                parts.Length == 3
                && Convert.ToInt64(parts[1]) == userId
                && DateTime.UtcNow < SimpleTypesExtensions.ToDateTime(Convert.ToInt64(parts[2]))
                && token.Equals(((IAuthenticationService)this).GenerateToken(userId, requestOrigin, expiresTimeStamp).AccessToken, StringComparison.InvariantCulture);
        }

        bool IAuthenticationService.CheckToken(AuthToken obj)
        {
            return ((IAuthenticationService)this).CheckToken(obj.UserId, obj.RequestOrigin, obj.UnixTimeStamp, obj.AccessToken);
        }

        AuthToken IAuthenticationService.Login(UserDetailsRepresentation loginData, string requestOrigin)
        {
            return ((IAuthenticationService)this).Login(loginData, requestOrigin, 15);
        }

        AuthToken IAuthenticationService.Login(UserDetailsRepresentation loginData, string requestOrigin, int expireAfterMinutes)
        {
            var existingUser = _userRepository.Find(x => x.Email == loginData.Email);
            var expire = DateTime.UtcNow.AddMinutes(expireAfterMinutes);
            if (existingUser != null && BCryptHelper.CheckPassword(loginData.Password, existingUser.Password))
            {
                return ((IAuthenticationService)this).GenerateToken(existingUser.Id, requestOrigin, expire.ToUnixTimeStamp());
            }
            throw new BadRequestException("Invalid user email or password");
        }
    }
}
