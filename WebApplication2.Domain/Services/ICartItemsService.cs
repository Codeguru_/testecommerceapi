﻿using System.Collections.Generic;
using WebApplication2.Core.Representations;

namespace WebApplication2.Domain.Services
{
    public interface ICartItemsService
    {
        CartItemRepresentation AddOrUpdate(long userCartId, long itemId, int quantity);
        IEnumerable<CartItemRepresentation> GetAll(long userCartId);
        void Remove(long userCartId, long itemId);
        void RemoveAll(long userCartId);
    }
}
