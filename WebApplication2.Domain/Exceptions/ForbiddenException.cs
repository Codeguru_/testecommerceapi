﻿using System;
using System.Runtime.Serialization;
using System.Web;

namespace WebApplication2.Domain.Exceptions
{

    [Serializable]
    public class ForbiddenException : HttpException
    {
        public ForbiddenException()
        {
        }

        public ForbiddenException(string message) : base(message)
        {
        }

        public ForbiddenException(string message, int hr) : base(message, hr)
        {
        }

        public ForbiddenException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public ForbiddenException(int httpCode, string message) : base(httpCode, message)
        {
        }

        public ForbiddenException(int httpCode, string message, Exception innerException) : base(httpCode, message, innerException)
        {
        }

        public ForbiddenException(int httpCode, string message, int hr) : base(httpCode, message, hr)
        {
        }

        protected ForbiddenException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}