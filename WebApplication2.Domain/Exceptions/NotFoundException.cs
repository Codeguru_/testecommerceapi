﻿using System;
using System.Runtime.Serialization;
using System.Web;

namespace WebApplication2.Domain.Exceptions
{

    [Serializable]
    public class NotFoundException : HttpException
    {
        public NotFoundException() : base (404, "resource was not found")
        {
        }

        public NotFoundException(string message) : base(404, message)
        {
        }

        public NotFoundException(string message, int hr) : base(404, message, hr)
        {
        }

        public NotFoundException(string message, Exception innerException) : base(404, message, innerException)
        {
        }

        protected NotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}