﻿using System;
using System.Runtime.Serialization;
using System.Web;

namespace WebApplication2.Domain.Exceptions
{

    [Serializable]
    public class BadRequestException : HttpException
    {
        public BadRequestException()
        {
        }

        public BadRequestException(string message) : base(message)
        {
        }

        public BadRequestException(string message, int hr) : base(message, hr)
        {
        }

        public BadRequestException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public BadRequestException(int httpCode, string message) : base(httpCode, message)
        {
        }

        public BadRequestException(int httpCode, string message, Exception innerException) : base(httpCode, message, innerException)
        {
        }

        public BadRequestException(int httpCode, string message, int hr) : base(httpCode, message, hr)
        {
        }

        protected BadRequestException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}