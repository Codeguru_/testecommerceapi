﻿using System;
using SimpleInjector;
using SimpleInjector.Packaging;
using WebApplication2.Domain.Services;

namespace WebApplication2.Domain.SimpleInjector
{
    public class DomainSimpleInjectorPackage : IPackage
    {
        public void RegisterServices(Container container)
        {
            container.Register<ICartItemsService, CartItemsService>();
            container.Register<IAuthenticationService, AuthenticationService>();
            container.Register<IAccountService, AccountService>();
        }
    }
}
